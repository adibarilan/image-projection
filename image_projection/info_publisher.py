#!/usr/bin/env python
"""
pointgrey_camera_driver (at least the version installed with apt-get) doesn't
properly handle camera info in indigo.
This node is a work-around that will read in a camera calibration .yaml
file (as created by the cameracalibrator.py in the camera_calibration pkg),
convert it to a valid sensor_msgs/CameraInfo message, and publish it on a
topic.

The yaml parsing is courtesy ROS-user Stephan:
    http://answers.ros.org/question/33929/camera-calibration-parser-in-python/

This file just extends that parser into a rosnode.
"""

import rospy
import yaml
from sensor_msgs.msg import CameraInfo

front_yaml = """
image_width: 720
image_height: 480
camera_name: camera/front
camera_matrix:
  rows: 3
  cols: 3
  data: [397.839423, 0, 379.601533, 0, 353.826068, 228.803869, 0, 0, 1]
distortion_model: plumb_bob
distortion_coefficients:
  rows: 1
  cols: 5
  data: [-0.313010, 0.078271, 0.000708, -0.000595, 0]
rectification_matrix:
  rows: 3
  cols: 3
  data: [1, 0, 0, 0, 1, 0, 0, 0, 1]
projection_matrix:
  rows: 3
  cols: 4
  data: [284.895752, 0, 376.271920, 0, 0, 290.811798, 223.900344, 0, 0, 0, 1, 0]
"""

back_yaml = """
image_width: 720
image_height: 480
camera_name: camera/back
camera_matrix:
  rows: 3
  cols: 3
  data: [397.839423, 0, 379.601533, 0, 353.826068, 228.803869, 0, 0, 1]
distortion_model: plumb_bob
distortion_coefficients:
  rows: 1
  cols: 5
  data: [-0.313010, 0.078271, 0.000708, -0.000595, 0]
rectification_matrix:
  rows: 3
  cols: 3
  data: [1, 0, 0, 0, 1, 0, 0, 0, 1]
projection_matrix:
  rows: 3
  cols: 4
  data: [284.895752, 0, 376.271920, 0, 0, 290.811798, 223.900344, 0, 0, 0, 1, 0]
"""



def yaml_to_CameraInfo(yaml_content):
    """
    Parse a yaml file containing camera calibration data (as produced by 
    rosrun camera_calibration cameracalibrator.py) into a 
    sensor_msgs/CameraInfo msg.
    
    Parameters
    ----------
    yaml_fname : str
        Path to yaml file containing camera calibration data

    Returns
    -------
    camera_info_msg : sensor_msgs.msg.CameraInfo
        A sensor_msgs.msg.CameraInfo message containing the camera calibration
        data
    """
    # Load data from file
    calib_data = yaml.load(yaml_content)
    # Parse
    camera_info_msg = CameraInfo()
    camera_info_msg.width = calib_data["image_width"]
    camera_info_msg.height = calib_data["image_height"]
    camera_info_msg.K = calib_data["camera_matrix"]["data"]
    camera_info_msg.D = calib_data["distortion_coefficients"]["data"]
    camera_info_msg.R = calib_data["rectification_matrix"]["data"]
    camera_info_msg.P = calib_data["projection_matrix"]["data"]
    camera_info_msg.distortion_model = calib_data["distortion_model"]
    return camera_info_msg

if __name__ == "__main__":

    # Parse yaml file
    front_camera_info_msg = yaml_to_CameraInfo(front_yaml)
    back_camera_info_msg = yaml_to_CameraInfo(back_yaml)

    # Initialize publisher node
    rospy.init_node("camera_info_publisher", anonymous=True)
    front_publisher = rospy.Publisher("camera/front/camera_info", CameraInfo, queue_size=10)
    back_publisher = rospy.Publisher("camera/back/camera_info", CameraInfo, queue_size=10)
    rate = rospy.Rate(10)

    # Run publisher
    while not rospy.is_shutdown():
        front_publisher.publish(front_camera_info_msg)
        back_publisher.publish(back_camera_info_msg)
        rate.sleep()